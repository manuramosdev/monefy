<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Visualizer for Monefy CSV files">
	<meta name="author" content="manuelramosdev">
	<title>Monefy Visualizer</title>
	<link rel="stylesheet" type="text/css" href="bootstrap.min.css">
	<script type="text/javascript" src="jquery.min.js"></script>
	<script type="text/javascript" src="bootstrap.min.js"></script>	
</head>
<body>
	<h1>Monefy Visualizer</h1>
	<div class="container">
		% include('moves.tpl')
	</div>
</body>
</html>
