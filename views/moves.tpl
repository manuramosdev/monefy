<table>
	<thead>
		<tr>
			<th>{{headers.date}}</th>
			<th>{{headers.account}}</th>
			<th>{{headers.category}}</th>
			<th>{{headers.description}}</th>
			<th>{{headers.amount}}</th>
		</tr>
	</thead>
	% for move in moves:
	<tr>
		<td>{{move.date}}</td>
		<td>{{move.account}}</td>
		<td>{{move.category}}</td>
		<td>{{move.description}}</td>
		<td>{{move.amount}}</td>
	</tr>
	% end
</table>
