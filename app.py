from monefyCSV import importCSV
from bottle import route, run, template, static_file, Bottle, get

app = Bottle()

@app.get('/<filename:re:.*\.css>')
def stylesheets(filename):
	return static_file(filename, root='static/assets/css')

@app.route('/<filename:re:.*\.js>')
def send_js(filename):
	return static_file(filename, root=dirname+'static/assets/js')

@app.route('/')
def index():
	filename = 'datapast.csv'
	moves = importCSV(filename)
	headers = moves[0]
	moves = moves[1:len(moves)]
	return template('index', moves=moves, headers=headers)

run(app,host='localhost', port=8080)
